package com.keysoft.bucktrackerjpa.dao;

import com.keysoft.bucktrackerjpa.entity.Application;

import java.util.List;

public interface IApplicationDAO {
    List<Application> getAllApplications();
    void addApplication(Application application);
    Application getApplicationById(int applicationId);
    void updateApplication(Application application);
    void deleteApplication(int applicationId);
    boolean applicationExists(String name, String owner);
}
